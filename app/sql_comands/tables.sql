CREATE TABLE users (
	id SERIAL PRIMARY KEY,
	username VARCHAR(50) UNIQUE NOT NULL,
	password VARCHAR(50) NOT NULL,
	email VARCHAR(255) UNIQUE NOT NULL,
	created_on TIMESTAMP NOT NULL,
    last_login TIMESTAMP
);

INSERT INTO users(username,password,email,created_on) 
VALUES(
	'olzasamirkulov',
	'olzhas3123213',
	'vip0amirkulov@gmail.com',
	current_timestamp
);
SELECT * FROM users;