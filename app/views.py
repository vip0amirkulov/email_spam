from app import app
from app.db_conn import User
from flask import render_template



@app.route("/")
def hello_world():
    print('--------------hello_world-----------------')
    return render_template('index.html', title = 'Home')


@app.route('/users')
def users():
    print('--------------users-----------------')
    users_list = []
    users = User.query.all()
    print('--------------query all-----------------')
    for user in users:
        users_list.append({
            'id': user.id,
            'username': user.username,
            'password': user.password,
            'email': user.email,
            'created_on': user.created_on
        })
    print('--------------users_list-----------------')
    print(users_list)

    

    return render_template('users.html', users=users_list)